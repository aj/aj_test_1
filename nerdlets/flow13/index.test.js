const navigation = require('navigation'); // Assuming you have a navigation module

describe('openFlowsDetailsPage', () => {
  it('should call navigation.getOpenStackedNerdletLocation with the correct nerdlet object', () => {
    // Arrange
    const transactionName = 'Transaction 1';
    const customerServiceName = 'Customer Service 1';
    const flowName = 'Flow 1';
    const entityGuid = '12345';
    const accountId = '67890';
    const expectedNerdlet = {
      id: 'transaction13',
      urlState: {
        accountId: accountId,
        transactionName: transactionName,
        entityGuid: entityGuid,
        customerServiceName: customerServiceName,
        flowName: flowName
      },
    };

    // Mock the console.log function
    console.log = jest.fn();

    // Mock the navigation.getOpenStackedNerdletLocation function
    navigation.getOpenStackedNerdletLocation = jest.fn();

    // Act
    openFlowsDetailsPage(transactionName, customerServiceName, flowName, entityGuid);

    // Assert
    expect(console.log).toHaveBeenCalledWith('[openFlowsDetailsPage] nerdlet', expectedNerdlet);
    expect(navigation.getOpenStackedNerdletLocation).toHaveBeenCalledWith(expectedNerdlet);
  });
});