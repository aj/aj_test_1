import React from 'react';
import {
  PlatformStateContext, navigation, nerdlet, Modal, BlockText, Button, NerdGraphQuery, List, ListItem, Grid, GridItem, HeadingText, AreaChart, TableChart, Table, TableHeader, Radio, SelectItem, Select, TableHeaderCell, TableRow, TableRowCell, AccountPicker, MetricTableRowCell
} from 'nr1';
import { timeRangeToNrql } from '@newrelic/nr1-community';

// https://docs.newrelic.com/docs/new-relic-programmable-platform-introduction

export default class Nr1HowtoAddTimePicker extends React.Component {
    constructor(props){
        super(props)
        this.since = 'SINCE 30 minutes ago';
        this.accountId = 3613936
        this.state = {
          customerServices: [],
          flows: [],
          accountId: null,
          since: 'SINCE 30 minutes ago',
        }
        this.onChangeAccount = this.onChangeAccount.bind(this);
        
    }

    onChangeAccount(_, value) {
      console.log('[CS Nerdlet][onChangeAccount] Selected account :', value);
      this.setUpDataForNerdlet("AccountChanged", value);
    }

    setUpDataForNerdlet(event, eventData) {
      if (event == "PlatformStateContext") {
        console.log('[CS Nerdlet][setUpDataForNerdlet] invoked due to PlatformStateContext update :', eventData);
        this.since = timeRangeToNrql(eventData);
      }
      if (event == "AccountChanged") {
        console.log('[CS Nerdlet][setUpDataForNerdlet] invoked due to PlatformStateContext update :', eventData);
        this.accountId = eventData;
      }

      const gql1 = `{
        actor {
          account(id: ` + this.accountId + `) {
            name
            nrql(query: "SELECT UNIQUES(name) FROM lookup(CustomerService) LIMIT MAX") {
              results
            }
          }
        }
      }`;

      

      const customerServicesQuery =  NerdGraphQuery.query({query: gql1})
      customerServicesQuery.then(results => {
          console.log('[componentDidMount] Nerdgraph Response gql1:', results);
          const customerServices = results.data.actor.account.nrql.results[0]['uniques.name']
          console.log('[componentDidMount] Customer Services:', customerServices);
          const commaSeparatedCustomerServices = '\'' + customerServices.join('\', \'') + '\'';
          console.log('[componentDidMount] : commaSeparatedCustomerServices: ', commaSeparatedCustomerServices);
          const flowsQuerySql = `FROM Transaction JOIN (FROM lookup(CustomerService) SELECT name as customer_service, tier, flow, latency_ms, transaction_name where name IN (` + commaSeparatedCustomerServices + `)) ON name = transaction_name SELECT count(*) AS 'TOTAL', percentage(count(*), WHERE error is false) AS 'Success Rate', percentage(count(*), WHERE http.statusCode < 500) AS 'Availability', percentage(count(*), WHERE duration <= latency_ms/1000) AS 'Latency' FACET customer_service, tier, flow, name, entityGuid LIMIT 500` + this.since;

          console.log('[componentDidMount] : flowsQuerySql: ', flowsQuerySql);
      
          const gql2 = `{
            actor {
              account(id: ` + this.accountId + `) {
                name
                nrql(query: "`+ flowsQuerySql +`") {
                  results
                }
              }
            }
          }`;
          const flowsQuery =  NerdGraphQuery.query({query: gql2})
          flowsQuery.then(results => {
            console.log('[componentDidMount] Nerdgraph Response gql2:', results);
            const flowsDataFromAPI = results.data.actor.account.nrql.results;
            const flowsData = flowsDataFromAPI.map((obj) => ({
              ...obj, 
              location: navigation.getOpenStackedNerdletLocation({
                id: 'transaction13',
                urlState: {
                    accountId: this.accountId,
                    transactionName: obj.facet[3],
                    entityGuid: obj.facet[4],
                    customerServiceName: obj.facet[0],
                    flowName: obj.facet[2],
                },
              }), 
            }));
            console.log('[componentDidMount] flowsData:', flowsData);
            this.setState({ customerServices: customerServices, flows: flowsData, accountId: this.accountId });
          }).catch((error) => { console.log('Nerdgraph Error gql2:', error); })
          
      }).catch((error) => { console.log('Nerdgraph Error gql1:', error); })
    }

    componentDidMount(){

        PlatformStateContext.subscribe((platformState)=>{
          console.log('[CS Nerdlet][componentDidMount] PlatformState :', platformState);
          this.setUpDataForNerdlet("PlatformStateContext", platformState);
        }); 
        
    }

    //Write a function to calculate fibonacci series  
    fibonacci(number){
      if(number < 2) {

        return number;
      } 
      else {
        return this.fibonacci(number-1) + this.fibonacci(number - 2);
      }
    }
    
    _getItems() {
        return [
            {
                team: 'Backend',
                company: 'Comtest',
                name: 'Web Portal',
                alertSeverity: 'CRITICAL',
                reporting: true,
                value: 0.9202394,
                commit: '0f58ef',
            },
            {
                team: 'Frontend',
                company: 'Comtest',
                name: 'Promo Service',
                alertSeverity: 'CRITICAL',
                reporting: true,
                value: 0.9123988,
                commit: 'e10fb3',
            },
            {
                team: 'DB',
                company: 'Comtest',
                name: 'Tower Portland',
                alertSeverity: 'CRITICAL',
                reporting: true,
                value: 0.82331,
                commit: 'ff8b07a',
            },
        ];
    }

    _getActions() {
        return [
            {
                label: 'Alert Team',
                iconType: TableRow.ACTIONS_ICON_TYPE.INTERFACE__OPERATIONS__ALERT,
                onClick: (evt, { item, index }) => {
                    alert(`Alert Team: ${item.team}`);
                },
            },
            {
                label: 'Rollback Version',
                iconType: TableRow.ACTIONS_ICON_TYPE.INTERFACE__OPERATIONS__UNDO,
                onClick: (evt, { item, index }) => {
                    alert(`Rollback from: ${item.commit}`);
                },
            },
        ];
    }

    openAPMEntity() {
      navigation.openStackedEntity('Mzk4MDUxOHxFWFR8S0VZX1RSQU5TQUNUSU9OfDEwMTM3NjQyNTQ2NzQ2MjQzMTM1');
    }

    /**
     * Opens the flows details page.
     * 
     * @param {string} transactionName - The name of the transaction.
     * @param {string} customerServiceName - The name of the customer service.
     * @param {string} flowName - The name of the flow.
     * @param {string} entityGuid - The GUID of the entity.
     * @returns {void}
     */
    openFlowsDetailsPage(transactionName, customerServiceName, flowName, entityGuid) {
      const nerdlet = {
        id: 'transaction13',
        urlState: {
            accountId: this.state.accountId,
            transactionName: transactionName,
            entityGuid: entityGuid,
            customerServiceName: customerServiceName,
            flowName: flowName
        },
      }; 
      console.log('[openFlowsDetailsPage] nerdlet', nerdlet);
      navigation.getOpenStackedNerdletLocation(nerdlet);
    }

    render() {
        const customerServiceList = `SELECT count(*) FROM lookup(CustomerService) FACET name LIMIT MAX`;
        const { customerServices, flows, accountId  } = this.state;
        console.log('[render start] customerServices', customerServices);
        console.log('[render start] flows', flows);

        const nerdlet = {
          id: 'transaction13',
          urlState: {
              facet: 'appName',
              accountId: accountId,
          },
        };  

        const entityGuid = 'MzYxMzkzNnxBUE18QVBQTElDQVRJT058MTUzOTQyNzA3Mw';

        const launcher = {
          id: 'distributed-tracing',
        };

        const nerdlet1 = {
          id: 'distributed-tracing.detail',
          urlState: {
            traceId: 'ab6c4a89488694f6885a7de4341e802d',
            startTimeMs: 1694978155724,
            accountId: accountId,
            rootEntityGuid: 'MzYxMzkzNnxBUE18QVBQTElDQVRJT058MTUxNDY1MjQ3MA',
            entityGuid: 'MzYxMzkzNnxBUE18QVBQTElDQVRJT058MTUxNDY1MjQ3MA',
          },
        };
        
        const overlay = {
          id: 'nr1-core.search',
        };

        const launcher1 = {
          id: 'distributed-tracing',
          nerdlet: {
            id: 'nr1-core.home',
          },
          stackedNerdlets: [
            {
              id: 'distributed-tracing',
            },
          ],
        };
        
        //const location = navigation.openStackedEntity(entityGuid);

        //const location = navigation.getReplaceNerdletLocation(nerdlet1);
        const location = navigation.getOpenStackedNerdletLocation(nerdlet1);
        //const location = navigation.getOpenLauncherLocation(launcher1);
        //const location = navigation.getOpenStackedEntityLocation(entityGuid);
        //const location = navigation.getOpenOverlayLocation(overlay);
        //const location = navigation.getOpenNerdletLocation(nerdlet1);
        //const location = navigation.getOpenLauncherLocation(launcher1);
        //const location = navigation.getOpenEntityLocation(entityGuid);
        //const location = navigation.getOpenNerdletLocation(nerdlet1);
        
          return (
          <>
              <AccountPicker
                label="Account"
                labelInline
                value={accountId}
                onChange={this.onChangeAccount}
              />

              <Grid
              className="primary-grid"
              spacingType={[Grid.SPACING_TYPE.NONE, Grid.SPACING_TYPE.NONE]}
              >
                  <GridItem className="primary-content-container" columnSpan={3}>
                    <main className="primary-content full-height">
                      <HeadingText spacingType={[HeadingText.SPACING_TYPE.MEDIUM]} type={HeadingText.TYPE.HEADING_4}>
                          Customer Services
                      </HeadingText>
                      <Table fullWidth fullHeight items={customerServices}>
                        <TableHeader>
                            <TableHeaderCell>Customer Services</TableHeaderCell>
                        </TableHeader>
                        {({ item }) => (
                            <TableRow>
                                <TableRowCell style={{ whiteSpace: 'pre-wrap' }}>{item}</TableRowCell>
                            </TableRow>
                        )}
                    </Table>
                    </main>    

                  </GridItem>
                  <GridItem fullHeight fullWidth className="primary-content-container" columnSpan={9}>
                      <main className="primary-content full-height">
                      <HeadingText spacingType={[HeadingText.SPACING_TYPE.MEDIUM]} type={HeadingText.TYPE.HEADING_4}>
                          Flows
                      </HeadingText>
                        
                      <Table fullWidth fullHeight items={flows}>
                        <TableHeader>
                            <TableHeaderCell>Customer Service</TableHeaderCell>
                            <TableHeaderCell>Flow</TableHeaderCell>
                            <TableHeaderCell>Transaction Name</TableHeaderCell>
                            <TableHeaderCell>TOTAL</TableHeaderCell>
                            <TableHeaderCell>Success Rate</TableHeaderCell>
                            <TableHeaderCell>Availability</TableHeaderCell>
                            <TableHeaderCell>Latency</TableHeaderCell>
                            <TableHeaderCell>Action</TableHeaderCell>
                        </TableHeader>
                        {({ item }) => (
                            <TableRow>
                                <TableRowCell>{item.facet[0]}</TableRowCell>
                                <TableRowCell>{item.facet[2]}</TableRowCell>
                                <TableRowCell>{item.facet[3]}</TableRowCell>
                                <MetricTableRowCell
                                  type={MetricTableRowCell.TYPE.COUNT}
                                  value={item.TOTAL}
                                />
                                <MetricTableRowCell
                                  type={MetricTableRowCell.TYPE.PERCENTAGE}
                                  value={item['Success Rate']/100}
                                />
                                <MetricTableRowCell
                                  type={MetricTableRowCell.TYPE.PERCENTAGE}
                                  value={item.Availability/100}
                                />
                                <MetricTableRowCell
                                  type={MetricTableRowCell.TYPE.PERCENTAGE}
                                  value={item.Latency/100}
                                />
                                <Button
                                    to={item.location}
                                >
                                    Details
                                </Button>
                            </TableRow>
                        )}
                    </Table>
                      </main>
                  </GridItem>
              </Grid>
          </>
          );
    }
}