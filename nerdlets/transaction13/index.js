import React from 'react';
import PropTypes from 'prop-types';
import { NerdletStateContext, navigation, PlatformStateContext, NerdGraphQuery, Table, Grid, GridItem, ChartGroup, AreaChart, BarChart, LineChart, BillboardChart, Badge, HeadingText, Button, TableHeader, TableHeaderCell, TableRow, TableRowCell, EntityTitleTableRowCell, MetricTableRowCell  } from 'nr1';

export default class MyNerdlet extends React.Component {
    static propTypes = {
        launcherUrlState: PropTypes.object,
        width: PropTypes.number,
        height: PropTypes.number,
    };

    constructor(props) {
        super(props);
        this.state = {
            nerdletState: {},
            traces: [],
            probes: [],
          }
    }

    componentDidMount(){
        PlatformStateContext.subscribe((subscriber)=>{console.log('AJ PlatformStateContext :', subscriber)}); 
        NerdletStateContext.subscribe((nerdletState)=>{
            console.log('[flow nerdlet][componentDidMount] NerdletStateContext:', nerdletState);
            const traceQuerySql = `SELECT timestamp, trace.id, spanCount, backend.duration.ms, errorCount, entityCount  from DistributedTraceSummary where root.span.process.name = '`+ nerdletState.transactionName +`' LIMIT 10`;
            const probesQuerySql = `SELECT timestamp, if(result = 'SUCCESS', '🟢', '🔴') AS ProbeStatus FROM SyntheticCheck WHERE monitorName = 'county-ledger-event-processing' LIMIT MAX`;
            console.log('[flow nerdlet][componentDidMount] traceQuerySql:', traceQuerySql);
            const gql1 = `{
                actor {
                  account(id: ` + nerdletState.accountId + `) {
                    name
                    nrql(query: "`+ traceQuerySql +`") {
                      results
                    }
                  }
                }
              }`;
            const gql2 = `{
                actor {
                  account(id: ` + nerdletState.accountId + `) {
                    name
                    nrql(query: "`+ probesQuerySql +`") {
                      results
                    }
                  }
                }
              }`;
            const tracesQuery =  NerdGraphQuery.query({query: gql1})
            tracesQuery.then(results => {
                console.log('[flow nerdlet][componentDidMount] Nerdgraph response gql1:', results);
                const tracesDataFromAPI = results.data.actor.account.nrql.results;
                const tracesData = tracesDataFromAPI.map((obj) => ({
                    ...obj, 
                    location: navigation.getOpenStackedNerdletLocation({
                      id: 'distributed-tracing.detail',
                      urlState: {
                        traceId: obj["trace.id"],
                        startTimeMs: obj.timestamp,
                        accountId: nerdletState.accountId,
                        rootEntityGuid: nerdletState.entityGuid,
                        entityGuid: nerdletState.entityGuid,
                      },
                    }), 
                  }));
                console.log('[flow nerdlet][componentDidMount] Traces Data:', tracesData);
                
                const probesQuery =  NerdGraphQuery.query({query: gql2})
                probesQuery.then(results => {
                    console.log('[flow nerdlet][componentDidMount] Nerdgraph response gql2:', results);
                    const probesData = results.data.actor.account.nrql.results;
                    console.log('[flow nerdlet][componentDidMount] Probes Data:', probesData);
                    this.setState({ nerdletState: nerdletState, traces: tracesData, probes: probesData });
                }).catch((error) => { console.log('[flow nerdlet] Nerdgraph Error gql2:', error); })
              }).catch((error) => { console.log('[flow nerdlet] Nerdgraph Error gql1:', error); })
        }); 
    }

    render() {
        const { nerdletState, traces, probes  } = this.state;
        console.log('[flow nerdlet][render start] nerdletState', nerdletState);
        console.log('[flow nerdlet][render start] traces', traces);
        console.log('[flow nerdlet][render start] probes', probes);
        return (
            <PlatformStateContext.Consumer>
                {(platformState) => {
                    const accountId = nerdletState.accountId;
                    const { duration } = platformState.timeRange;
                    const since = ` SINCE ${duration/1000/60} MINUTES AGO `;
                    const rateQuery = `select count(*) AS RATE FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`' TIMESERIES`;
                    const errorQuery = `select count(*) AS ERRORS FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`' AND error is not false AND http.statusCode >= 500 AND http.statusCode < 600 TIMESERIES`;
                    const durationQuery = `select average(duration) AS DURATION FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`' TIMESERIES `;
                    const totalQuery = `select count(*) AS 'TOTAL' FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`'`;
                    const successQuery = `select count(*) AS 'Success' FROM Transaction where transactionType = 'Web' AND http.statusCode >= 200 AND http.statusCode < 300 AND name = '`+ nerdletState.transactionName +`'`;
                    const error4xxQuery = `select count(*) AS '4xx' FROM Transaction where transactionType = 'Web' AND http.statusCode >= 400 AND http.statusCode < 500 AND name = '`+ nerdletState.transactionName +`'`;
                    const error5xxQuery = `select count(*) AS '5xx' FROM Transaction where transactionType = 'Web' AND http.statusCode >= 500 AND http.statusCode < 600 AND name = '`+ nerdletState.transactionName +`'`;
                    const successRate = `select percentage(count(*), WHERE error is false) AS 'Success Rate' FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`'`;
                    const availabilityRate = `select percentage(count(*), WHERE http.statusCode < 500) AS 'Availability' FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`'`;
                    const latencyRate = `select percentage(count(*), WHERE duration <= 1.0) AS 'Latency' FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`'`;
                    const latency99th = `select percentile(duration, 99) AS 'Latency (s)' FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`'`;
                    const latency95th = `select percentile(duration, 95) AS 'Latency (s)' FROM Transaction where transactionType = 'Web' AND name = '`+ nerdletState.transactionName +`'`;
                    console.log('[flow nerdlet][render start] rateQuery:', rateQuery);

                    const transactionNerdlet = {
                        id: 'apm-features.transaction-drilldown',
                        urlState: {
                            accountId: nerdletState.accountId,
                            agentLanguage: "java",
                            drilldown: {
                                transactionName: nerdletState.transactionName,
                            },
                            selectedInstance: null,
                            sort: "sum",
                            transactionType: {
                                transactionType: "Web"
                            },
                            historicalPerformanceTab: {
                                compareWith: "YESTERDAY"
                            },
                            entityGuid: nerdletState.entityGuid,
                        },
                      };
                    const transactionLocation = navigation.getOpenStackedNerdletLocation(transactionNerdlet);

                    return <React.Fragment>
                        <ChartGroup>
                            <Grid>
                                <GridItem columnSpan={6}>
                                    <HeadingText type={HeadingText.TYPE.HEADING_5}>Flow - {nerdletState.flowName}</HeadingText>
                                    <HeadingText type={HeadingText.TYPE.HEADING_6}>Transaction Name - {nerdletState.transactionName}</HeadingText>
                                    <HeadingText type={HeadingText.TYPE.HEADING_6}>Customer Service - {nerdletState.customerServiceName}</HeadingText>
                                </GridItem>
                                <GridItem columnSpan={6}>
                                    <Button to={transactionLocation}>View Transaction Details</Button>
                                </GridItem>
                                <GridItem columnSpan={4}>
                                    <LineChart
                                        query={rateQuery+since}
                                        accountId={accountId}
                                        className="chart"
                                        onClickLine={(line) => {
                                            console.debug(line); //eslint-disable-line
                                        }}
                                    />
                                </GridItem>
                                <GridItem columnSpan={4}>
                                    <LineChart
                                        query={errorQuery+since}
                                        accountId={accountId}
                                        className="chart"
                                        onClickLine={(line) => {
                                            console.debug(line); //eslint-disable-line
                                        }}
                                    />
                                </GridItem>
                                <GridItem columnSpan={4}>
                                    <LineChart
                                        query={durationQuery+since}
                                        accountId={accountId}
                                        className="chart"
                                        onClickLine={(line) => {
                                            console.debug(line); //eslint-disable-line
                                        }}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={totalQuery+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={successQuery+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={error4xxQuery+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={error5xxQuery+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={successRate+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={availabilityRate+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={latencyRate+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={latency99th+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem columnSpan={1}>
                                    <BillboardChart
                                        query={latency95th+since}
                                        accountId={accountId}
                                    />
                                </GridItem>
                                <GridItem fullHeight fullWidth className="primary-content-container" columnSpan={3}>
                                    <main className="primary-content full-height">
                                        <HeadingText spacingType={[HeadingText.SPACING_TYPE.MEDIUM]} type={HeadingText.TYPE.HEADING_4}>
                                            Probes
                                        </HeadingText>
                                        
                                        <Table fullWidth fullHeight items={probes}>
                                            <TableHeader>
                                                <TableHeaderCell>Result</TableHeaderCell>
                                            </TableHeader>
                                            {({ item }) => (
                                                <TableRow>
                                                    <Badge type={Badge.TYPE.SUCCESS}>{item.ProbeStatus}</Badge>
                                                </TableRow>
                                            )}
                                        </Table>
                                    </main>
                                </GridItem>
                                <GridItem fullHeight fullWidth className="primary-content-container" columnSpan={9}>
                                    <main className="primary-content full-height">
                                        <HeadingText spacingType={[HeadingText.SPACING_TYPE.MEDIUM]} type={HeadingText.TYPE.HEADING_4}>
                                            Traces
                                        </HeadingText>
                                        
                                        <Table fullWidth fullHeight items={traces}>
                                            <TableHeader>
                                                <TableHeaderCell>Trace Id</TableHeaderCell>
                                                <TableHeaderCell>Span Count</TableHeaderCell>
                                                <TableHeaderCell>Duration(ms)</TableHeaderCell>
                                                <TableHeaderCell>Error Count</TableHeaderCell>
                                                <TableHeaderCell>Entity Count</TableHeaderCell>
                                                <TableHeaderCell>Action</TableHeaderCell>
                                            </TableHeader>
                                            {({ item }) => (
                                                <TableRow>
                                                    <TableRowCell>{item["trace.id"]}</TableRowCell>
                                                    <TableRowCell>{item.spanCount}</TableRowCell>
                                                    <TableRowCell>{item["backend.duration.ms"]}</TableRowCell>
                                                    <MetricTableRowCell
                                                        type={MetricTableRowCell.TYPE.COUNT}
                                                        value={item.errorCount}
                                                    />
                                                    <MetricTableRowCell
                                                        type={MetricTableRowCell.TYPE.PERCENTAGE}
                                                        value={item.entityCount}
                                                    />
                                                    <Button to={item.location}
                                                    >
                                                        View Trace
                                                    </Button>
                                                </TableRow>
                                            )}
                                        </Table>
                                    </main>
                                </GridItem>
                                
                            </Grid>
                        </ChartGroup>
                    </React.Fragment>;
                }}
            </PlatformStateContext.Consumer>
        )       
    }
}